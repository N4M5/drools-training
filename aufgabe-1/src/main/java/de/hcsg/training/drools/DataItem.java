package de.hcsg.training.drools;

import java.util.Objects;

public class DataItem {
    private String name;
    private double doubleValue;
    private boolean booleanValue;

    public DataItem() {
        // Empty constructor
    }

    public DataItem(final String name, final double doubleValue) {
        this.name = name;
        this.doubleValue = doubleValue;
    }

    public DataItem(final String name, final boolean booleanValue) {
        this.name = name;
        this.booleanValue = booleanValue;
    }

    public boolean isBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(final boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(final double doubleValue) {
        this.doubleValue = doubleValue;
    }

    @Override
    public String toString() {
        return "DataItem{" +
                "name='" + name + '\'' +
                ", doubleValue=" + doubleValue +
                ", booleanValue=" + booleanValue +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final DataItem dataItem = (DataItem) o;
        return Double.compare(dataItem.doubleValue, doubleValue) == 0 && booleanValue == dataItem.booleanValue && Objects
                .equals(name, dataItem.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, doubleValue, booleanValue);
    }
}
