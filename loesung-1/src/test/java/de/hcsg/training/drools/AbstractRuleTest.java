package de.hcsg.training.drools;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public abstract class AbstractRuleTest {
    private static KieContainer kContainer;
    protected KieSession kSession;

    @BeforeClass
    public static void bootstrapRuleEngine() {
        KieServices ks = KieServices.Factory.get();
        kContainer = ks.getKieClasspathContainer();
    }

    @Before
    public void initSession() {
        kSession = kContainer.newKieSession();
    }

    @After
    public void closeSession() {
        kSession.dispose();
    }
}
