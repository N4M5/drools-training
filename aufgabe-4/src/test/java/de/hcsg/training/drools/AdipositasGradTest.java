package de.hcsg.training.drools;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.kie.api.runtime.ClassObjectFilter;

import java.util.Collection;

@RunWith(Parameterized.class)
public class AdipositasGradTest extends AbstractRuleTest {

    @Parameters
    public static Object[][] parameters() {
        return new Object[][]{
                {50, 2, "Untergewicht"},
                {80, 2, "Normalgewicht"},
                {100, 2, "Übergewicht"},
                {134, 2, "Adipositas Grad I"},
                {144, 2, "Adipositas Grad II"},
                {164, 2, "Adipositas Grad III"},
        };
    }

    private final double gewicht;
    private final double groesse;
    private final String expectedAdipositasGrad;

    public AdipositasGradTest(double gewicht, double groesse, String expectedAdipositasGrad) {
        this.gewicht = gewicht;
        this.groesse = groesse;
        this.expectedAdipositasGrad = expectedAdipositasGrad;
    }

    @Test
    public void testAdipositasGrad() {
        final DataItem gewichtDataItem = new DataItem("gewicht", gewicht);
        final DataItem groesseDataItem = new DataItem("groesse", groesse);

        kSession.insert(gewichtDataItem);
        kSession.insert(groesseDataItem);
        kSession.fireAllRules();

        final DataItem adipositasGrad = new DataItem("adipositasGrad", expectedAdipositasGrad);
        Assertions.assertThat((Collection) kSession.getObjects(new ClassObjectFilter(DataItem.class))).contains(adipositasGrad);
    }

}
