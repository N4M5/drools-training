package de.hcsg.training.drools;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.kie.api.KieServices;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public abstract class AbstractRuleTest {
    private static final boolean DO_EXPLICIT_CHECK = false;
    private static final boolean ENABLE_EVENT_LISTENERS = false;
    private static KieContainer kContainer;
    protected KieSession kSession;

    @BeforeClass
    public static void bootstrapRuleEngine() {
        KieServices ks = KieServices.Factory.get();
        kContainer = ks.getKieClasspathContainer();
        if (DO_EXPLICIT_CHECK) {
            final Results results = kContainer.verify();
            if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)) {
                System.err.println("Rule compilation failed:");
                for (final Message message : results.getMessages()) {
                    System.err.println(ToStringBuilder.reflectionToString(message));
                }
                throw new RuntimeException("Rule compilation failed");
            }
        }
    }

    @Before
    public void initSession() {
        kSession = kContainer.newKieSession();
        if (ENABLE_EVENT_LISTENERS) {
            kSession.addEventListener(new DebugAgendaEventListener());
            kSession.addEventListener(new DebugRuleRuntimeEventListener());
        }
    }

    @After
    public void closeSession() {
        kSession.dispose();
    }
}
