package de.hcsg.training.drools;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.kie.api.runtime.ClassObjectFilter;

import java.util.Collection;

public class BmiTest extends AbstractRuleTest {

    @Test
    public void testBmi() {
        final DataItem gewicht = new DataItem("gewicht", 100.0);
        final DataItem groesse = new DataItem("groesse", 2.0);

        kSession.insert(gewicht);
        kSession.insert(groesse);
        int fired = kSession.fireAllRules();

        System.out.println("Number of Rules executed = " + fired);

        final DataItem bmi = new DataItem("bmi", 25.0);
        Assertions.assertThat((Collection) kSession.getObjects(new ClassObjectFilter(DataItem.class))).contains(bmi);
    }
}
