package de.hcsg.training.drools;

import java.util.Objects;

public class DataItem {
    private String name;
    private double doubleValue;
    private boolean booleanValue;
    private String stringValue;

    public DataItem() {
        // Empty constructor
    }

    public DataItem(final String name, final double doubleValue) {
        this.name = name;
        this.doubleValue = doubleValue;
    }

    public DataItem(final String name, final boolean booleanValue) {
        this.name = name;
        this.booleanValue = booleanValue;
    }

    public DataItem(final String name, final String stringValue) {
        this.name = name;
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(final String stringValue) {
        this.stringValue = stringValue;
    }

    public boolean isBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(final boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(final double doubleValue) {
        this.doubleValue = doubleValue;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final DataItem dataItem = (DataItem) o;
        return Double.compare(dataItem.doubleValue, doubleValue) == 0 && booleanValue == dataItem.booleanValue && Objects
                .equals(name, dataItem.name) && Objects.equals(stringValue, dataItem.stringValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, doubleValue, booleanValue, stringValue);
    }

    @Override
    public String toString() {
        return "DataItem{" +
                "name='" + name + '\'' +
                ", doubleValue=" + doubleValue +
                ", booleanValue=" + booleanValue +
                ", stringValue='" + stringValue + '\'' +
                '}';
    }

}
