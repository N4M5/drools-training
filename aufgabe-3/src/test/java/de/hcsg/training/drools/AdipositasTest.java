package de.hcsg.training.drools;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.kie.api.runtime.ClassObjectFilter;

import java.util.Collection;

public class AdipositasTest extends AbstractRuleTest {

    @Test
    public void testAdipositas() {
        final DataItem gewicht = new DataItem("gewicht", 132.0);
        final DataItem groesse = new DataItem("groesse", 2.0);

        kSession.insert(gewicht);
        kSession.insert(groesse);
        int fired = kSession.fireAllRules();

        System.out.println("Number of Rules executed = " + fired);

        final DataItem adipositas = new DataItem("adipositas", true);
        Assertions.assertThat((Collection) kSession.getObjects(new ClassObjectFilter(DataItem.class))).contains(adipositas);
    }

    @Test
    public void testKeinAdipositas() {
        final DataItem gewicht = new DataItem("gewicht", 90.0);
        final DataItem groesse = new DataItem("groesse", 2.0);

        kSession.insert(gewicht);
        kSession.insert(groesse);
        int fired = kSession.fireAllRules();

        System.out.println("Number of Rules executed = " + fired);

        final DataItem adipositas = new DataItem("adipositas", true);
        Assertions.assertThat((Collection) kSession.getObjects(new ClassObjectFilter(DataItem.class))).doesNotContain(adipositas);
    }
}
