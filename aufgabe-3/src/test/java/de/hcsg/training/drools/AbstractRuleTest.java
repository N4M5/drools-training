package de.hcsg.training.drools;

import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.io.FileNotFoundException;

public abstract class AbstractRuleTest {
    private static KieContainer kContainer;
    protected KieSession kSession;

    @BeforeClass
    public static void bootstrapRuleEngine() throws FileNotFoundException {
        if (kContainer == null) {
            KieServices ks = KieServices.Factory.get();
            kContainer = ks.getKieClasspathContainer();
            printXlsAsDrl();
        }
    }

    private static void printXlsAsDrl() throws FileNotFoundException {
        SpreadsheetCompiler spreadsheetCompiler = new SpreadsheetCompiler();
        String drl = spreadsheetCompiler.compile(AbstractRuleTest.class.getResourceAsStream("/rules.xls"), InputType.XLS);
        System.out.println("Rules derived from spreadsheet");
        System.out.println("------------------------------");
        System.out.println(drl);
    }

    @Before
    public void initSession() {
        kSession = kContainer.newKieSession();
    }

    @After
    public void closeSession() {
        kSession.dispose();
    }
}
